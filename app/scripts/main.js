var App = Ember.Application.create({
	title: "my title",
	
// Controllers --------------------------------------------------------------------
	ApplicationController: Ember.Controller.extend({
		title: "controller title"
	}),

// Views ---------------------------------------------------------------------------
	// 10 pool ---------------------------------------------------
	testView: Ember.View.extend({
		name: 'jordy',
		layout: Ember.Handlebars.compile("{{view.name}}")
	}),
	tenpoolView: Ember.CollectionView.extend({
		ugly: true,
		magic: "unicorns",
		race: "Terran",
		strat: "4 gate",
		content: [
			{timing: 'num3', name: 'drone'},
			{timing: 'num5', name: 'drone'},
			{timing: 'num7', name: 'pool'},
			{timing: 'num9', name: 'gas'}
		],
		itemViewClass: Ember.View.extend({
			classNames: ['scunit'],
			classNameBindings: ['content.timing', 'content.name'],
			template: Ember.Handlebars.compile("{{view.content.name}}")
		})
	}),

	fourrackView: Ember.CollectionView.extend({
		ugly: true,
		magic: "unicorns",
		race: "Terran",
		strat: "4 gate",
		content: [
			{timing: 'num3', name: 'scv'},
			{timing: 'num5', name: 'scv'},
			{timing: 'num7', name: 'supply'},
			{timing: 'num9', name: 'baraks'}
		],
		itemViewClass: Ember.View.extend({
			classNames: ['scunit'],
			classNameBindings: ['content.timing', 'content.name'],
			template: Ember.Handlebars.compile("{{view.content.name}}")
		})
	}),

	fourgateView: Ember.CollectionView.extend({
		ugly: true,
		magic: "unicorns",
		race: "Terran",
		strat: "4 gate",
		content: [
			{timing: 'num3', name: 'probe'},
			{timing: 'num5', name: 'probe'},
			{timing: 'num7', name: 'stargate'},
			{timing: 'num9', name: 'gas'}
		],
		itemViewClass: Ember.View.extend({
			classNames: ['scunit'],
			classNameBindings: ['content.timing', 'content.name'],
			template: Ember.Handlebars.compile("{{view.content.name}}")
		})
	}),
	bansheeView: Ember.CollectionView.extend({
		ugly: true,
		race: "Terran",
		tagName: 'ul',
		content: [
			{timing: 'num3', name: 'scv'},
			{timing: 'num7', name: 'scv'},
			{timing: 'num12', name: 'scv'},
			{timing: 'num15', name: 'supply'},
			{timing: 'num20', name: 'barraks'},
			{timing: 'num23', name: 'gas'},
			{timing: 'num27', name: 'scv'},
			{timing: 'num32', name: 'starport'},
			{timing: 'num35 stop', name: 'Done!'}
		],
		itemViewClass: Ember.View.extend({
			classNames: ['scunit'],
			classNameBindings: ['content.timing', 'content.name'],
			template: Ember.Handlebars.compile("{{view.content.name}}")
		}),
	}),


// Controller --------------------------------------------------------------------

	StratController: Ember.Controller.extend({
		strat: 'controller stat',
		count: 0,
		resetVars: function(){
			this.set('isFourrack', false);
			this.set('isBanshee', false);
		},
		zergScreen: function(){
			this.set('isZergBuilds', true);
			this.set('isTerranBuilds', false);
		},
		terranScreen: function(){
			this.set('isTerranBuilds', true);
			this.set('isZergBuilds', false);
		},
		fourrack: function(){
			this.resetVars();
			this.set('isFourrack', true);
		},
		banshee: function(){
			this.resetVars();
			this.set('isBanshee', true);
		},
		startTime: function() {
			this.loop();
		},
		loop: function(){
			Ember.run.later(this, function(){
				this.incrementProperty('count');
				var curCount = this.get('count');
				if(!$('.num' + curCount).hasClass('stop')){
					if($('.num' + curCount)[0]){
						$('.scunit').fadeOut();
						$('.num' + curCount).fadeIn();
					};
					this.loop();
				}
			}, 1000);
		}
	})

});

	// routing ---------------
	App.Router.map(function() {
		this.route('strat');
	});
